<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
use dosamigos\datetimepicker\DateTimePicker;

/* @var $this yii\web\View */

$this->title = 'Управление автотранспортным хранилищем';
?>

<header class="masthead text-white text-center" style="height: calc(100vh);">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-xl-9 mx-auto">
				<h2 class="mb-5">Введите данные и получите код талона для парковки</h2>
			</div>
			<div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
				<div class="col-12 col-md-12 mb-2 mb-md-0">
					<?php $form = ActiveForm::begin() ?>
					<div class="form-row">
						<div class="col-12">
							<?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder' => $model->getAttributeLabel( 'username' ), 'class' => 'form-control form-control-lg'])->label(false) ?>
						</div>
						<div class="col-12">
							<?= $form->field($model, 'number')->textInput(['placeholder' => $model->getAttributeLabel( 'number' ), 'class' => 'form-control form-control-lg'])->label(false) ?>
						</div>
						<div class="col-6 mx-auto mt-1">
							<?= Html::submitButton(Yii::t('app', 'Получить талон'), ['class' =>  'btn btn-block btn-lg btn-primary', 'style' => 'width: 200px; height: 50px;display: block;margin: 10px auto 0;']) ?>
						</div>
					</div>
					<?php ActiveForm::end() ?>
				</div>
			</div>
		</div>
	</div>
</header>
