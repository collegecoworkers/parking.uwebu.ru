<?php

use yii\helpers\Html;

$this->title = 'Спасибо за использование нашего сервиса';

?>

<header class="masthead text-white text-center" style="height: calc(100vh);">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<h2 class="mb-5">Код вашего талона: <?= $code ?></h2>
			</div>
		</div>
	</div>
</header>
