<?php

use yii\db\Migration;

class m171004_101859_parking extends Migration
{
    public function safeUp()
    {
        $this->createTable('parking', [
            'id' => $this->primaryKey(),
            'username' => $this->string(),
            'number' => $this->string(10),
            'code' => $this->string(20),
            'status' => $this->integer()->defaultValue(0),
        ]);
    }

    public function safeDown()
    {
        return false;
    }
}
