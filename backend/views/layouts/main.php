<?php

use backend\assets\AppAsset;
use common\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);

$modules_id = Yii::$app->controller->module->id;
$controller_id = Yii::$app->controller->id;
$action_id = Yii::$app->controller->action->id;
$entity_id = isset($_GET['id']) ? (int)$_GET['id'] : null;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <section id="top-navbar">
        <div class="nav fixed-top">
            <!-- navbar left -->
            <div class="nav-left" style="width: 300px;">
                <a href="/admin" class="brand">Админ панель</a>
            </div>
            <!-- navbar right -->
            <div class="nav-right">
                <div class="right">
                <?php if (!Yii::$app->user->isGuest): ?>

                    <div class="user dropdown">
                        <a href="#" class="btn" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">A</a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="<?= Url::base() ?>/site/logout"><i class="zmdi zmdi-input-power"></i> Выход</a>
                        </div>
                    </div>
                    <div class="user message" style="margin-right: 60px;">
                        <a class="btn" href="<?= Url::base() ?>/site/all">Все заявки</a>
                    </div>
                    <div class="user notifications" style="margin-right: 100px;">
                        <a class="btn" href="<?= Url::base() ?>/">Главная</a>
                    </div>
                <?php endif ?>
                </div>
            </div>
        </div>
    </section>
    <div id="layout"  >
        <?= $content ?>
    </div>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>