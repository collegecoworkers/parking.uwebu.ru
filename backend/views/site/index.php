<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

$this->title = Yii::t('app', 'Автотранспортное хранилище');
$this->params['breadcrumbs'][] = $this->title;
$status = '';
?>
<div class="col-md-12" style="color: black">
	<div class="panel panel-default">
		<br><br><br>
		<div class="panel-heading"><?= $this->title ?></div>
		<div class="panel-body">
			<div class="contact-index">
				<div class="fa-br"></div>
				<br>
				<?php
				$dataProvider = new ActiveDataProvider([
					'query' => $model,
					'pagination' => [
						'pageSize' => 20,
					],
				]);
				echo GridView::widget([
					'dataProvider' => $dataProvider,
					'layout' => "{items}\n{pager}",
					'columns' => [
			// ['class' => 'yii\grid\SerialColumn'],
						'id',
						'username',
						'number',
						'code',
						[
							'label' => 'Статус',
							'attribute' => 'status',
							'format' => 'raw',
							'value' => function($dataProvider){
								return ($dataProvider->status == 0) ? 'На парковке' : 'Выехал';
							},
						],
						[
							'label' => 'Действие',
							'format' => 'raw',
							'value' => function($dataProvider){
								return Html::a("Выехал", ['site/update', 'id' => $dataProvider->id]);
							},
						],
					],
				]);
				?>

			</div>

		</div>
	</div>
</div>
