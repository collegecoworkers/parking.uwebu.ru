<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'src/plug/bootstrap.css',
        'src/dist/style/app.css',
    ];
    public $js = [
        'src/plug/jquery.js',
        'src/plug/tether.js',
        'src/plug/bootstrap.js',
        'src/dist/js/app.js',
    ];
    public $depends = [
        // 'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
        // 'yii\bootstrap\BootstrapPluginAsset',
    ];
}
