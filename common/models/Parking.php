<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Parking model
 *
 * @property integer $id
 * @property string $email
 */
class Parking extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%parking}}';
    }

    public function behaviors()
    {
        return [
        ];
    }

    public function attributeLabels() {
        return [
            'username' => 'Имя',
            'number' => 'Номер автомобиля',
            'code' => 'Код талона',
            'status' => 'Статус',
        ];
    }

    public function rules()
    {
        return [
            [['username', 'number'], 'required'],
            [['username', 'number'], 'string']
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

}
